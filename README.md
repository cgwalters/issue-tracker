# Issue tracker for project Sagano

This is the repo to track work in progress and issues for the project Sagano.

## What's the goal of this project

This project's goal is to create bootable container images from Fedora ELN and
CentOS Stream packages using technologies such as ostree, rpm-ostree, bootc,
podman, buildah, skopeo.

The objective is to create a container native operating system that is similar
in spirit to what Fedora CoreOS, RHEL CoreOS, Fedora IoT and RHEL 4 Edge are
today, but focused on container native workflows.

## What does Sagano means?

From [Wikipedia](https://en.wikipedia.org/wiki/Bamboo_Forest_(Kyoto,_Japan)):

> Bamboo Forest, Arashiyama Bamboo Grove or Sagano Bamboo Forest, is a natural
> forest of bamboo in Arashiyama, Kyoto, Japan
